#----------------------------------------------------------------------------------------------------------------------
# rect_filter.py
# 
# a simple program to generate a csv file with the numeric values of a filtered-rectangle wave 
#
# author: Moritz Mossböck
# email : moritz.mossboeck@gmail.com
#----------------------------------------------------------------------------------------------------------------------
#imports
import math
import cmath

# variable declarations
i = 1
step = 0.01
t = -5
f = open("rect_filter.csv","w")
wg = 2
w0 = 0.5*math.pi

#----------------------------------------------------------------------------------------------------------------------
# function: coeff
#
# returns the fourier-coefficient for the filtered rectangle-wave
#----------------------------------------------------------------------------------------------------------------------
def coeff(k):
  return math.sin(math.pi*0.5*k)/(2*math.pi*k)

#----------------------------------------------------------------------------------------------------------------------
# function: phasor
#
# returns the complex number representing the filter-function
#----------------------------------------------------------------------------------------------------------------------
def phasor(k):
  return 1/(1+1j*((k*w0)/wg))

#----------------------------------------------------------------------------------------------------------------------
# operation
#----------------------------------------------------------------------------------------------------------------------
while t <= 5:
  func_val = 0
  i = 1
  while i <= 80:
    func_val = func_val + ( coeff(i)*phasor(i)*cmath.exp(1j*i*w0*t) + coeff(-i)*phasor(-i)*cmath.exp(-1j*i*w0*t) )
    i = i + 1

  func_val = func_val + 0.5 #coefficient of i=0

  f.write(str(t)+","+str(func_val.real)+"\n")
  t = t + step