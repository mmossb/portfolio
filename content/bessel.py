#----------------------------------------------------------------------------------------------------------------------
# bessel.py
# 
# a simple program to generate plots (and numeric tables) of bessel functions
#
# author: Moritz Mossböck
# email : moritz.mossboeck@gmail.com
#----------------------------------------------------------------------------------------------------------------------
#imports
import numpy as np               # mostly used for array-stuff
import matplotlib.pyplot as plt  # used for plotting calculated data
import scipy.special             # provides the bessel functions
import sympy                     # used for UTF8 printing in terminal
import argparse                  # used for easy CLI-arg parsing
import csv                       # used for easy creating/manipulation of csv files

# inital setup
plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = r'\usepackage{lmodern}'

# argument setup
parser=argparse.ArgumentParser()

parser.add_argument("--order", "-o", default=[1],nargs='+',help="set bessel-function order (can be a list)")
parser.add_argument("--range","-r", default=10,nargs='?', help="the range for the function",type=float)
parser.add_argument("--step","-s", default=0.5,nargs='?', help="increment for stepping from 0 to <range>", type=float)
parser.add_argument("--scale",default=1,nargs='?', help="scale values", type=float)
parser.add_argument("--plot",default=False,nargs='?',help="whether a plot should be shown",type=bool)
parser.add_argument("--export","-e",default=False,nargs='?',help="whether a plot should be exported",type=bool)
parser.add_argument("--format",default='pdf',nargs='?',help="the target format for plot exports")
parser.add_argument("--name",default='Bessel-Functions',nargs='?',help="name for the plot",type=str)
parser.add_argument("--verbose","-v",default=False,nargs='?',
                    help="whether all information should be printed to CLI",type=bool)
parser.add_argument("--csv",default=False,nargs='?',
                    help="whether a csv file with all values should be exported",type=bool)


# advanced setup
vals = parser.parse_args()
x = np.arange(0,vals.range+vals.step, vals.step)

# initialize pretty UTF8 printing in CLI
sympy.init_printing()

#----------------------------------------------------------------------------------------------------------------------
# calculation
#----------------------------------------------------------------------------------------------------------------------
data = []

for i in vals.order:
  num = []
  tmp = vals.scale*scipy.special.jve(int(i),x)
  for j in range(len(tmp)):
    tmp[j] = int(100*tmp[j])
    tmp[j] = tmp[j]/100.0

  for j in range(len(tmp)):
    num.append(tmp[j])
  data.append(num)

#----------------------------------------------------------------------------------------------------------------------
# output: CLI
#----------------------------------------------------------------------------------------------------------------------
if vals.verbose:
  eta = sympy.symbols('eta')
  sympy.pprint(eta,)
  for i in range(len(x)):
    print('{:6}'.format(x[i]),end="")

  print("")
  for i in range(len(vals.order)):
    string = "J_{}".format(vals.order[i])
    sym = sympy.symbols(string)
    sympy.pprint(sym,)
    for j in range(len(tmp)):
      print('{:6}'.format(data[i][j]),end="")

    print("")

#----------------------------------------------------------------------------------------------------------------------
# output: show plot (and save plot, if set)
#----------------------------------------------------------------------------------------------------------------------
if (vals.plot):
  for i in range(len(data)):
    plt.plot(x,data[i],label='$J_{'+str(vals.order[i])+'}$')

  plt.legend(title='',loc='upper right',bbox_to_anchor=(1.2,0.85),
  fancybox=True,borderaxespad=-5)
  plt.xlabel(r'$\eta$')
  plt.ylabel(r'$J_n(\eta)$')
  plt.grid()
  plt.title('$\mathrm{'+vals.name+'}$')
  plt.tight_layout()
  if (vals.export):
    plt.savefig(f'{vals.name}.{vals.format}')
  plt.show()

#----------------------------------------------------------------------------------------------------------------------
# output: only save plot
#----------------------------------------------------------------------------------------------------------------------
if vals.export and not vals.plot:
  for i in range(len(data)):
    plt.plot(x,data[i],label='$J_{'+str(vals.order[i])+'}$')

  plt.legend(title='',loc='upper right',bbox_to_anchor=(1.2,0.85),
  fancybox=True,borderaxespad=-5)
  plt.xlabel(r'$\eta$')
  plt.ylabel(r'$J_n(\eta)$')
  plt.grid()
  plt.title('$\mathrm{'+vals.name+'}$')
  plt.tight_layout()
  plt.savefig(f'{vals.name}.{vals.format}')

#----------------------------------------------------------------------------------------------------------------------
# output: save numeric values in csv file
#----------------------------------------------------------------------------------------------------------------------
if vals.csv:
  with open(f'{vals.name}.csv',"w+") as ecsv:
    csvWriter = csv.writer(ecsv)
    args = np.array(vals.order)
    args = np.insert(args,0,0)
    csvWriter.writerow(args)
    tmp = [[0]*len(x)]*(len(data)+1)
    for j in range(len(x)):
      tmp[0][j] = x[j]

    for i in range(len(data)):
      tmp.append(data[i])

    csvWriter.writerows(np.array(tmp).T)
  ecsv.close()
