### Provided Content

The following is a short explanation for everything in the `content` directory:
* bessel.py, a small program to generate plots of bessel functions
* rect_filter.py, used for generating numeric values of a filtered rectangle wave
* bsp2.pdf, part of a homework assignment
* dua_blatt_01.pdf is a homework for the course Data-Structures and Algorithms

### List of public projects

I am currently actively working in these projects:
* [latin-is-simple](latin-is-simple.com), an online latin-dictionary
* [prettytex](https://github.com/MrP01/prettytex), a collection of Latex-Classes and Styles
